#! /usr/bin/env python

import os
import schedule as sch
from typing import List
import datetime
from pytz import UTC, timezone
import time
import subprocess
import signal
from glob import glob
import logging

recorder_dir = os.path.dirname(os.path.realpath(__file__))
capture_dir = ''
with open(f'{recorder_dir}/output.txt') as output_file:
    capture_dir = output_file.read()

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s %(message)s",
    handlers=[
        logging.FileHandler(f'{capture_dir}/recorder.log'),
        logging.StreamHandler()
    ]
)


def load_schedule() -> List[sch.MeetEvent]:

    sched = sch.get_schedule(
        url=sch.get_cal_url(recorder_dir),
        ignore_list=sch.get_list(recorder_dir, 'ignore')
    )

    today = sch.get_today_classes(
        schedule=sched,
        priority_list=sch.get_list(recorder_dir, 'priority')
    )

    return today


def get_time_offset(h: int = 0, m: int = 0, s: int = 0) -> datetime.datetime.time:
    return (datetime.datetime.now(timezone('Europe/Belgrade')) + datetime.timedelta(hours=h, minutes=m, seconds=s)).time()


def wait_loop(events: List[sch.MeetEvent]):

    logging.info(f'Schedule for {get_time_offset().strftime("%A")}:')
    for e in events:
        if not e.skip:
            logging.info(f'- {e.start} {e.end} {e.subject}')

    logging.info('Starting OBS')
    proc_obs = start_obs()
    time.sleep(5)

    for e in events:
        if e.skip == False and (e.start >= get_time_offset() or \
            (e.start <= get_time_offset(m=+5) and e.end >= get_time_offset())):

            logging.info(f'Waiting for \'{e.subject}\' at {e.start}')
            while get_time_offset(m=+5) < e.start:
                time.sleep(10)
            
            proc_meeting = start_meeting(e.url)
            time.sleep(60)

            join_meeting()
            time.sleep(10)
        
            toggle_recording(True)
            logging.info(f'Recording \'{e.subject}\'')

            while get_time_offset(m=-8) < e.end:
                time.sleep(10)

            logging.info(f'Closing \'{e.subject}\'')
            toggle_recording(False)

            os.killpg(proc_meeting.pid, signal.SIGTERM)
            proc_meeting.wait()

            time.sleep(20)
            rename_video(e)

        else:
            logging.info(f'Skipping \'{e.subject}\' at {e.start}')

    logging.info('Reached the end of the schedule')
    time.sleep(30)
    
    logging.info('Stopping OBS')
    os.killpg(proc_obs.pid, signal.SIGTERM)
    proc_obs.wait()


def start_obs() -> subprocess.Popen:

    os.system('killall -q obs')
    os.system('bspc desktop --focus 8.local')

    proc = subprocess.Popen(
        'obs',
        shell=True,
        preexec_fn=os.setsid,
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL
    )

    time.sleep(10)

    return proc


def start_meeting(url: str) -> subprocess.Popen:
    
    os.system('killall -q google-chrome-stable')
    os.system('bspc desktop --focus 9.local')

    proc = subprocess.Popen(
        f'google-chrome-stable --kiosk {url}',
        shell=True,
        preexec_fn=os.setsid,
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL
    )

    return proc


def toggle_recording(start: bool):

    _key = 'r' if start else 't'

    os.system(f' \
        xdotool keydown ctrl keydown alt keydown {_key} \
                sleep 1 \
                keyup {_key} keyup alt keyup ctrl \
    ')


def join_meeting():

    os.system(' \
        xdotool mousemove 680 470 click 1 \
                sleep 5 \
                mousemove 896 457 click 1 \
                sleep 5 \
                mousemove 825 610 click 1 \
                sleep 1 \
                mousemove 730 610 click 1 sleep 1 mousemove 730 440 click 1 \
                sleep 1 \
                mousemove 515 385 click 1 \
    ')


def rename_video(e: sch.MeetEvent):

    files = glob(f'{capture_dir}/OBS_CAPTURE_*')

    _date = sch.get_time().date()

    for f in range(len(files)):
        os.rename(
            files[f],
            f'{capture_dir}/{_date} {e.start} {e.end} {e.subject}.{f}.mkv'
        )


if __name__ == '__main__':

    wait_loop(load_schedule())
