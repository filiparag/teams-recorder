#!/bin/sh

dir=$(dirname "$(readlink -f -- "$0")")

tee ~/.config/wmrc/rc.conf << END
%WM = bspwm
%TERMINAL = alacritty
%FILEMGR = pcmanfm
%BROWSER = google-chrome-stable

[init]
wm/bspwm
hid/keyboard,sxhkd
screen/layout(defined,'Default')
ui/polybar
services/recorder

[reload]
wm/bspwm(workspaces)
hid/sxhkd(reload)
ui/polybar(restart)
END

tee ~/.config/wmrc/modules/services/recorder << END
#! /usr/bin/env dash
# WMRC_DEPS: python
# WMRC_FLAGS:

start() {
    bspc desktop --focus 0.local
    \$TERMINAL -e /recorder/recorder.py &
    pid="\$!"
    echo "\$pid" > /tmp/recorder.pid
}

stop() {
    xdotool keydown ctrl
    xdotool keydown alt
    xdotool keydown t
    sleep 1
    xdotool keyup t
    xdotool keyup alt
    xdotool keyup ctrl
    kill \$(cat /tmp/recorder.pid)
}

. "\$WMRC_MODULES/init"
END
chmod +x ~/.config/wmrc/modules/services/recorder

sudo tee /etc/X11/xorg.conf.d/10-monitor.conf << END
Section "Extensions"
    Option      "DPMS" "Disable"
EndSection
END

grep '/recorder' -q /etc/fstab || {
    sudo tee -a /etc/fstab << END
/data		/data		9p	trans=virtio,rw		0 0
/recorder	/recorder	9p	trans=virtio,ro		0 0
END
}

crontab -l | grep -q recorder || {
    sudo tee -a "/var/spool/cron/$(whoami)" < "$dir/recorder.cron"
}

tee ~/.screenlayout/Default.sh << END
#!/bin/sh
xrandr --output Virtual-1 --primary --mode 1360x768 --pos 0x0 --rotate normal
END

yay -S obs-studio google-chrome --needed --noconfirm

pip install --user -r "$dir/requirements.txt"

sudo mkdir -p /data/ftn
sudo cp -r "$dir" /recorder
sudo chmod +x /recorder/recorder.py
sudo chown -R "$(whoami):$(whoami)" /data/ftn

patch -p0 -d ~ < "$dir/obs-studio.patch"