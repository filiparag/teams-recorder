#!/usr/bin/env python

from dataclasses import dataclass
from typing import List
from icalendar import Calendar, Event
import datetime
import urllib.request
from pytz import UTC, timezone
from pprint import pprint
import operator
import re
import os
import sys
import time
import logging

@dataclass
class MeetEvent:
    code: str
    subject: str
    day: int
    start: datetime.time
    end: datetime.time
    url: str
    skip: bool

    def __eq__(self, other):
        return self.code == other.code and self.day == other.day \
            and self.start == other.start

    def __hash__(self):
        return hash((self.code, self.day, self.start))


def get_cal_url(dir: str) -> str:
    with open(f'{dir}/url.txt') as url_file:
        return url_file.read()


def get_time() -> datetime.datetime:
    return datetime.datetime.now(timezone('Europe/Belgrade'))


def get_list(dir: str, name: str) -> List[str]:
    with open(f'{dir}/{name}_list.txt') as list_file:
        return list_file.read().splitlines()


def get_meet_url(url_part: str) -> str:
    return f'https://teams.microsoft.com/_#/l/meetup-join/19:meeting_{url_part}&anon=true'


def get_schedule(url: str, ignore_list: List[str]) -> List[MeetEvent]:

    ics_text = None
    with urllib.request.urlopen(url) as ics_file:
        ics_text = ics_file.read()

    gcal = Calendar.from_ical(ics_text)

    events: List[MeetEvent] = []

    for component in gcal.walk():
        if component.name == "VEVENT":

            _name = str(component.get('summary'))
            _desc = component.get('description')
            _start = component.get('dtstart').dt
            _end = component.get('dtend').dt
            
            _meet = re.findall("meeting_([a-zA-Z0-9]+[^>]*)", _desc)
            if len(_meet) > 0:
                _meet = _meet[0]
            else:
                _meet = ''
            
            _code = _name.split(' ')[0]
            if _code in ignore_list:
                continue

            _subject = ' '.join(_name.split(' ')[1:])
            _meet_code = _meet.replace('%40','@')
            _day = _start.weekday()

            events.append(MeetEvent(
                _code, _subject, _day, _start.time(), _end.time(), _meet_code, False
            ))

    _urls = {}

    for e in events:
        if len(e.url) > 0:
            _urls[e.code] = e.url

    for e in events:
        if len(e.url) == 0:
            if e.code in _urls: # Temporary fix for events without subject code
                e.url = _urls[e.code]
            else:
                e.skip = True

    return sorted(set(events), key=operator.attrgetter('start'))


def get_today_classes(schedule: List[MeetEvent], priority_list: List[str]) -> List[MeetEvent]:
    
    events: List[MeetEvent] = []
    now = get_time()

    for e in schedule:
        if e.day == now.weekday():
            events.append(MeetEvent(
                e.code,
                e.subject,
                e.day,
                e.start,
                e.end,
                get_meet_url(e.url),
                e.skip
            ))

    for e1 in range(len(events)):
        for e2 in range(e1 + 1, len(events)):
            if events[e2].start <= events[e1].end:
                logging.info(f'Conflict: \'{events[e1].subject}\' with \'{events[e2].subject}\'')
                if events[e1].code in priority_list and events[e2].code in priority_list:
                    p1 = priority_list.index(events[e1].code)
                    p2 = priority_list.index(events[e2].code)
                    if p2 < p1:
                        logging.info(f'Attending second event: \'{events[e2].subject}\'')
                        events[e1].skip = True
                    else:
                        logging.info(f'Attending first event: \'{events[e1].subject}\'')
                        events[e2].skip = True
                elif events[e2].code in priority_list:
                    logging.info(f'Attending second event: \'{events[e2].subject}\'')
                    events[e1].skip = True
                else:
                    logging.info(f'Attending first event: \'{events[e1].subject}\'')
                    events[e2].skip = True

    return events
