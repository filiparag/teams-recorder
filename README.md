# Microsoft Teams Recorder

## Installation

Required packages for manual installation:

- python ≥ `3.8`
- xdotool ≥ `3.0`
- obs-studio ≥ `26.0`
- google-chrome ≥ `86.0`
- bspwm ≥ `0.9`

### KVM Virtual machine

This program is intended to run in a customized Arch Linux environment, which can be obtained [here](https://github.com/filiparag/dotfiles/tree/b83b31bcc99604999529a10918b53f73f8b1ca92). Follow installation script and use default values.

Recommended virtual machine configuration for virt-manager QEMU can be found [here](./teams-recorder.xml).

To install this script, run [`install.sh`](./install.sh).

## Usage

### Importing meetings from Outlook calendar

Set [`url.txt`](./url.txt) to value found on https://outlook.office365.com/mail/options/calendar/SharedCalendars.


### Recording path and priority lists

To ignore a meeting, add it to `ignore_list.txt`.

If multiple meetings are held at the same time, priority goes to the first found in `priority_list.txt`.
Meetings without defined priority have lowest priority by default.

Set `output.txt` to reflect path configured in obs-studio.